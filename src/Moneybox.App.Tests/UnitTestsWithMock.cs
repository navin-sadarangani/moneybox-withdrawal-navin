﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using Moneybox.App.Features;
using Moq;
using System;

namespace Moneybox.App.Tests
{
    [TestClass]
    public class UnitTestsWithMock
    {
        private Mock<IAccountRepository> _accountRepoMock;
        private Mock<INotificationService> _notificationSvcMock;


        public UnitTestsWithMock()
        {
            _accountRepoMock = new Mock<IAccountRepository>();
            _accountRepoMock.Setup(s => s.GetAccountById(TestData.DefaultFromAccount.Id)).Returns(TestData.DefaultFromAccount);
            _accountRepoMock.Setup(s => s.GetAccountById(TestData.DefaultToAccount.Id)).Returns(TestData.DefaultToAccount);

            _notificationSvcMock = new Mock<INotificationService>();
            _notificationSvcMock.Setup(s => s.NotifyFundsLow(TestData.DefaultFromAccount.User.Email));
        }

        [TestMethod]
        public void WithdrawSufficientBalance()
        {
            //arrange
            WithdrawMoney withdrawMoney = new WithdrawMoney(_accountRepoMock.Object, _notificationSvcMock.Object);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal withrawAmount = 250m;

            //act
            withdrawMoney.Execute(TestData.DefaultFromAccount.Id, withrawAmount);

            //assert
            Assert.AreEqual(balance - withrawAmount, TestData.DefaultFromAccount.Balance);

            //assert notifications raised
            _notificationSvcMock.Verify(Mock => Mock.NotifyFundsLow(TestData.DefaultFromAccount.User.Email), Times.Never());
        }


        [TestMethod]
        public void WithdrawEntireBalance()
        {

            //arrange
            WithdrawMoney withdrawMoney = new WithdrawMoney(_accountRepoMock.Object, _notificationSvcMock.Object);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal withrawAmount = balance;

            //act
            withdrawMoney.Execute(TestData.DefaultFromAccount.Id, withrawAmount);

            //assert
            Assert.AreEqual(balance - withrawAmount, TestData.DefaultFromAccount.Balance);
            Assert.AreEqual(withrawAmount, TestData.DefaultFromAccount.Withdrawn);

            //assert notifications raised
            _notificationSvcMock.Verify(Mock => Mock.NotifyFundsLow(TestData.DefaultFromAccount.User.Email), Times.Once());
        }


        [TestMethod]
        public void WithdrawLowThreshold()
        {

            //arrange
            WithdrawMoney withdrawMoney = new WithdrawMoney(_accountRepoMock.Object, _notificationSvcMock.Object);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal withrawAmount = balance-100m;

            //act
            withdrawMoney.Execute(TestData.DefaultFromAccount.Id, withrawAmount);

            //assert
            Assert.AreEqual(balance - withrawAmount, TestData.DefaultFromAccount.Balance);
            Assert.AreEqual(withrawAmount, TestData.DefaultFromAccount.Withdrawn);

            //assert notifications raised
            _notificationSvcMock.Verify(Mock => Mock.NotifyFundsLow(TestData.DefaultFromAccount.User.Email), Times.Once());
        }



        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void WithdrawMoreThanBalanceAndException()
        {
            //arrange
            WithdrawMoney withdrawMoney = new WithdrawMoney(_accountRepoMock.Object, _notificationSvcMock.Object);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal withrawAmount = balance+100m;

            //act
            withdrawMoney.Execute(TestData.DefaultFromAccount.Id, withrawAmount);

            //assert
            Assert.AreEqual(balance - withrawAmount, TestData.DefaultFromAccount.Balance);
            Assert.AreEqual(withrawAmount, TestData.DefaultFromAccount.Withdrawn);

            //assert notifications raised
            _notificationSvcMock.Verify(Mock => Mock.NotifyFundsLow(TestData.DefaultFromAccount.User.Email), Times.Once());

        }
       

        [TestMethod]
        public void TransferMoney_PayInSufficientBalance()
        {
            //arrange
            TransferMoney transferMoney = new TransferMoney(_accountRepoMock.Object, _notificationSvcMock.Object);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal payInAmount = 250m;

            //act
            transferMoney.Execute(TestData.DefaultFromAccount.Id, TestData.DefaultToAccount.Id, payInAmount);

            //assert
            Assert.AreEqual(balance - payInAmount, TestData.DefaultFromAccount.Balance);
            Assert.AreEqual(balance + payInAmount, TestData.DefaultToAccount.Balance);

            //assert notifications raised
            _notificationSvcMock.Verify(Mock => Mock.NotifyFundsLow(TestData.DefaultFromAccount.User.Email), Times.Never());
        }


        [TestMethod]
        public void TransferMoney_PaymentLimitNotification()
        {
            //arrange
            TransferMoney transferMoney = new TransferMoney(_accountRepoMock.Object, _notificationSvcMock.Object);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal payInAmount = Account.PayInLimit - 250m;

            //act
            transferMoney.Execute(TestData.DefaultFromAccount.Id, TestData.DefaultToAccount.Id, payInAmount);

            //assert
            Assert.AreEqual(balance - payInAmount, TestData.DefaultFromAccount.Balance);
            Assert.AreEqual(balance + payInAmount, TestData.DefaultToAccount.Balance);

            //assert notifications raised
            _notificationSvcMock.Verify(Mock => Mock.NotifyApproachingPayInLimit(TestData.DefaultToAccount.User.Email), Times.Once());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TransferMoney_LimitThresholdException()
        {
            //arrange
            TransferMoney transferMoney = new TransferMoney(_accountRepoMock.Object, _notificationSvcMock.Object);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal payInAmount = balance;

            //act
            transferMoney.Execute(TestData.DefaultFromAccount.Id, TestData.DefaultToAccount.Id, payInAmount);

            //assert - wont hit as exceptions is raised
            Assert.AreEqual(balance, TestData.DefaultFromAccount.Balance);
            Assert.AreEqual(balance, TestData.DefaultToAccount.Balance);

            //assert notifications raised
            _notificationSvcMock.Verify(Mock => Mock.NotifyFundsLow(TestData.DefaultFromAccount.User.Email), Times.Never());
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TransferMoney_InsufficientBalanceException()
        {
            //arrange
            TransferMoney transferMoney = new TransferMoney(_accountRepoMock.Object, _notificationSvcMock.Object);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal payInAmount = balance + 250m;

            //act
            transferMoney.Execute(TestData.DefaultFromAccount.Id, TestData.DefaultToAccount.Id, payInAmount);

            //assert
            Assert.AreEqual(balance - payInAmount, TestData.DefaultFromAccount.Balance);
            Assert.AreEqual(balance + payInAmount, TestData.DefaultToAccount.Balance);

            //assert notifications raised
            _notificationSvcMock.Verify(Mock => Mock.NotifyFundsLow(TestData.DefaultFromAccount.User.Email), Times.Never());
        }
    }
}
