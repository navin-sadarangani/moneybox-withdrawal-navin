﻿using System;
using Ninject;
using Ninject.Modules;

namespace Moneybox.App.Tests
{
    public class DependencyResolver : IDependencyResolver
    {
        public IKernel Kernel { get; }

        public DependencyResolver(params INinjectModule[] modules)
        {
            Kernel = new StandardKernel(modules);
            Kernel.Bind<IDependencyResolver>().ToConstant(this);
        }

        public T Resolve<T>()
        {
            return Kernel.Get<T>();
        }

        public T Resolve<T>(Type serviceType)
        {
            return (T)Kernel.Get(serviceType);
        }
    }
}
