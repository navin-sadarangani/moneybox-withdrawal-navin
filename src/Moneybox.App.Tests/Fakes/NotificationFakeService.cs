﻿using Moneybox.App.Domain.Services;
using System.Diagnostics;

namespace Moneybox.App.Tests.Fakes
{
    public class NotificationFakeService : INotificationService
    {
        public void NotifyApproachingPayInLimit(string emailAddress)
        {
            Debug.WriteLine($"{emailAddress} approaching pay in limit");            
        }

        public void NotifyFundsLow(string emailAddress)
        {
            Debug.WriteLine($"{emailAddress} funds low");
        }
    }
}
