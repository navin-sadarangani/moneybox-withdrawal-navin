﻿using Moneybox.App.DataAccess;
using System;

namespace Moneybox.App.Tests.Fakes
{
    public class AccountRepositoryFake : IAccountRepository
    {
        public Account GetAccountById(Guid accountId)
        {   
            TestData.DefaultFromAccount.Id = accountId;
            return TestData.DefaultFromAccount;
        }

        public void Update(Account account)
        {
            TestData.DefaultFromAccount = account;            
        }
    }
}
