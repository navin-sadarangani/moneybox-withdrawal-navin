﻿using System;

namespace Moneybox.App.Tests
{
    public interface IDependencyResolver
    {
        T Resolve<T>();
        T Resolve<T>(Type serviceType);
    }
}
