﻿using System;

namespace Moneybox.App.Tests
{
    public static class TestData
    {

        internal static decimal _balance = 50000m;
        internal static decimal _paidIn = 0m;
        internal static decimal _withdrawn = 0m;

        //List<Account> accountsInMemoryDatabase = new List<Account>
        //{
        //    new Account() {Id = Guid.NewGuid(), Balance = 50000m, PaidIn = 0m, Withdrawn = 0m, User = new User{Id = Guid.NewGuid(), Email = "test1@test.com", Name= "Test 1 user"}},
        //    new Account() {Id = Guid.NewGuid(), Balance = 50000m, PaidIn = 0m, Withdrawn = 0m, User = new User{Id = Guid.NewGuid(), Email = "test2@test.com", Name= "Test 2 user"}},
        //    new Account() {Id = Guid.NewGuid(), Balance = 50000m, PaidIn = 0m, Withdrawn = 0m, User = new User{Id = Guid.NewGuid(), Email = "test3@test.com", Name= "Test 3 user"}},
        //};


        public static Account DefaultFromAccount = new Account
        {
            Id = Guid.NewGuid(),
            Balance = _balance,
            PaidIn = _balance,
            Withdrawn = _withdrawn,
            User = new User { Id = Guid.NewGuid(), Email = "from@account.com", Name = "From Account" }
        };

        public static Account DefaultToAccount = new Account
        {
            Id = Guid.NewGuid(),
            Balance = _balance,
            PaidIn = _paidIn,
            Withdrawn = _withdrawn,
            User = new User { Id = Guid.NewGuid(), Email = "to@account.com", Name = "To Account" }
        };

        public static void ResetAccount()
        {
            DefaultFromAccount.Balance = _balance;
            DefaultFromAccount.PaidIn = _paidIn;
            DefaultFromAccount.Withdrawn = _withdrawn;

            DefaultToAccount.Balance = _balance;
            DefaultToAccount.PaidIn = _paidIn;
            DefaultToAccount.Withdrawn = _withdrawn;
        }
  
    }

}
