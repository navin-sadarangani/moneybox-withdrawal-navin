using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using Moneybox.App.Features;
using System;

namespace Moneybox.App.Tests
{
    [TestClass]
    public class UnitTestsWithDI
    {
        private DependencyResolver _dependencyResolver;
        private IAccountRepository _accountRepo;
        private INotificationService _notificationService;

        public UnitTestsWithDI()
        {
            _dependencyResolver = new DependencyResolver(new TestCaseUtilitiesIocModule());

            _accountRepo = _dependencyResolver.Resolve<IAccountRepository>();
            _notificationService = _dependencyResolver.Resolve<INotificationService>();

        }
         

        [TestMethod]
        public void WithdrawSufficientBalance()
        {
            //arrange
            WithdrawMoney withdrawMoney = new WithdrawMoney(_accountRepo, _notificationService);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal withrawAmount = 250m;

            //act
            withdrawMoney.Execute(Guid.NewGuid(), withrawAmount);

            //assert
            Assert.AreEqual(balance - withrawAmount, TestData.DefaultFromAccount.Balance);

        } 

        [TestMethod]
        public void WithdrawEntireBalance()
        {
            //arrange
            WithdrawMoney withdrawMoney = new WithdrawMoney(_accountRepo, _notificationService);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal withrawAmount = balance;

            //act
            withdrawMoney.Execute(Guid.NewGuid(), balance);                      

            //assert
            Assert.AreEqual(0, TestData.DefaultFromAccount.Balance);
             
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void WithdrawMoreThanBalanceAndException()
        {
            //arrange
            WithdrawMoney withdrawMoney = new WithdrawMoney(_accountRepo, _notificationService);

            TestData.ResetAccount();
            decimal balance = TestData.DefaultFromAccount.Balance;
            decimal withrawAmount = balance + 100m;

            //act
            withdrawMoney.Execute(TestData.DefaultFromAccount.Id, withrawAmount);

            //assert
            Assert.AreEqual(balance - withrawAmount, TestData.DefaultFromAccount.Balance);

        }


    }
}
