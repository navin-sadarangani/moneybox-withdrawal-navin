﻿using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using Moneybox.App.Tests.Fakes;
using Ninject.Modules;

namespace Moneybox.App.Tests
{
    public class TestCaseUtilitiesIocModule : NinjectModule
    {
        public override void Load()
        {
            Bind<INotificationService>().To<NotificationFakeService>().InSingletonScope();

            Bind<IAccountRepository>().To<AccountRepositoryFake>().InSingletonScope();

        }
    }
}
