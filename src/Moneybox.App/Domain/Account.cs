﻿using System;

namespace Moneybox.App
{
    public class Account
    {
        public const decimal PayInLimit = 4000m;

        //public const decimal WithdrawlLimit = 4000m;

        public Guid Id { get; set; }

        public User User { get; set; }

        public decimal Balance { get; set; }

        public decimal PaidIn { get; set; }


        public decimal Withdrawn { get; set; }
       

        public void PayIn(decimal amount)
        {
            if ((amount + PaidIn) > PayInLimit)
            {
                throw new InvalidOperationException("Account pay in limit reached");
            }
            else
            {
                PaidIn += amount;
                Balance += amount;
            }            
        }

        public void Withdrawl(decimal amount, bool transfer)
        {
            if ((Balance - amount) < 0m)
            {
                var trnType = transfer ? "transfer" : "withdraw";
                throw new InvalidOperationException($"Insufficient funds to {trnType}");
            }
            else
            {
                Withdrawn += amount;
                Balance = Balance - amount;                
            }

            //like the same, we can implement withdrawl limit condition, 
            //maybe better suited with a transaction history
        }




    }
}
