# Moneybox Money Withdrawal

The solution contains a .NET core library (Moneybox.App) which is structured into the following 3 folders:

* Domain - this contains the domain models for a user and an account, and a notification service.
* Features - this contains two operations, one which is implemented (transfer money) and another which isn't (withdraw money)
* DataAccess - this contains a repository for retrieving and saving an account (and the nested user it belongs to)

## The task

The task is to implement a money withdrawal in the WithdrawMoney.Execute(...) method in the features folder. For consistency, the logic should be the same as the TransferMoney.Execute(...) method i.e. notifications for low funds and exceptions where the operation is not possible. 

As part of this process however, you should look to refactor some of the code in the TransferMoney.Execute(...) method into the domain models, and make these models less susceptible to misuse. We're looking to make our domain models rich in behaviour and much more than just plain old objects, however we don't want any data persistance operations (i.e. data access repositories) to bleed into our domain. This should simplify the task of implementing WithdrawMoney.Execute(...).

## Guidelines

* You should spend no more than 1 hour on this task, although there is no time limit
* You should fork or copy this repository into your own public repository (Github, BitBucket etc.) before you do your work
* Your solution must compile and run first time
* You should not alter the notification service or the the account repository interfaces
* You may add unit/integration tests using a test framework (and/or mocking framework) of your choice
* You may edit this README.md if you want to give more details around your work (e.g. why you have done something a particular way, or anything else you would look to do but didn't have time)

Once you have completed test, zip up your solution, excluding any build artifacts to reduce the size, and email it back to our recruitment team.

Good luck!


## Notes from test
* Approach was to build testing capability first, validate if implementation works and then refactor later.
* would have taken extra effort to add AccountHistory to record withdrawal, this way tracking withdrawal limit per period (day/month/year) can be validated. In interest of time and effort, didn't proceed.
* didn't spend much time on mocking data - so currently a single static defaultAccount object which is common through all test cases with a Reset before every test case
* normally, would have DI container in its own separate project, for this test - just kept it together with the unit test project
* started using a DI container, but found it much work to assert notification service, so then opted for Mocking. Left both in the project. Again doing it later, might tidy up and keep only one.
* Finally for refactoring added to methods within the POCO object  account, which implements the withdraw or payin actions. Thought credit/debit would have been a better methodname to use after implementing it. hey ho!


